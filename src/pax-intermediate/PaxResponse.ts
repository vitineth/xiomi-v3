import { CookieOptions, Errback, Response } from 'express';
import { Engine } from 'liquidts';
import { Options, Template, Writeable } from 'liquidts/dist/types';

export class PaxResponse {

  private readonly expressResponse: Response;
  private listeners: { [key: string]: [(name: string, data: { [key: string]: any }) => void] };
  private liquid: Engine;

  constructor(expressResponse: Response, liquid: Engine) {
    this.expressResponse = expressResponse;
    this.liquid = liquid;
    this.listeners = {};
  }

  get app() {
    return this.expressResponse.app;
  }

  get headersSent() {
    return this.expressResponse.headersSent;
  }

  get locals() {
    return this.expressResponse.locals;
  }

  private notify(eventName: string, data: { [key: string]: any }) {
    if (this.listeners.hasOwnProperty(eventName)) {
      for (const executor of this.listeners[eventName]) executor(eventName, data);
    }
    if (this.listeners.hasOwnProperty('*')) {
      for (const executor of this.listeners['*']) executor(eventName, data);
    }
  }

  public listenFor(eventName: string, listener: (name: string, data: { [key: string]: any }) => void) {
    if (this.listeners.hasOwnProperty(eventName)) {
      this.listeners[eventName].push(listener);
    } else {
      this.listeners[eventName] = [listener];
    }
  }

  public append(field: string, value?: string[] | string): PaxResponse {
    this.expressResponse.append(field, value);
    this.notify('append', { field, value });
    return this;
  }

  public attachment(filename?: string): PaxResponse {
    this.expressResponse.attachment(filename);
    this.notify('attachment', { filename });
    return this;
  }

  public cookie(name: string, value: any, options?: CookieOptions): PaxResponse {
    if (options !== undefined) {
      this.expressResponse.cookie(name, value, options);
    } else {
      this.expressResponse.cookie(name, value);
    }
    this.notify('cookie', { name, value, options });
    return this;
  }

  public clearCookie(name: string, options?: CookieOptions): PaxResponse {
    this.expressResponse.clearCookie(name, options);
    this.notify('clearCookie', { name, options });
    return this;
  }

  public download(path: string, filename?: string, fn?: Errback): void {
    if (filename === undefined) {
      if (fn === undefined) {
        this.expressResponse.download(path);
      } else {
        this.expressResponse.download(path, fn);
      }
    } else {
      if (fn === undefined) {
        this.expressResponse.download(path, filename);
      } else {
        this.expressResponse.download(path, filename, fn);
      }
    }
    this.notify('download', { path, filename, fn });
  }

  public end(cb?: () => void): void {
    this.expressResponse.end(cb);
    this.notify('end', { cb });
  }

  public format(object: any): PaxResponse {
    this.expressResponse.format(object);
    this.notify('format', { object });
    return this;
  }

  public get(field: string): string {
    const result = this.expressResponse.get(field);
    this.notify('get', { field, result });
    return result;
  }

  public header(field: string, value?: string | string[]): string | PaxResponse {
    if (value === undefined) {
      return this.get(field);
    }
    return this.set(field, value);
  }

  public json(body?: any): PaxResponse {
    this.expressResponse.json(body);
    this.notify('json', { body });
    return this;
  }

  public jsonp(body?: any): PaxResponse {
    this.expressResponse.jsonp(body);
    this.notify('jsonp', { body });
    return this;
  }

  public links(links: any): PaxResponse {
    this.expressResponse.links(links);
    this.notify('links', { links });
    return this;
  }

  public location(path: string): PaxResponse {
    this.expressResponse.location(path);
    this.notify('location', { path });
    return this;
  }

  public redirect(url: string, status?: number): void {
    if (status === undefined) {
      this.expressResponse.redirect(url);
    } else {
      this.expressResponse.redirect(url, status);
    }
    this.notify('redirect', { url, status });
  }


  public render(target: string | Template | Template[], data?: object, options?: Options): Promise<PaxResponse> {
    return new Promise<PaxResponse>((resolve, reject) => {

      if (typeof (target) === 'string') {
        const result = this.liquid.parseAndRender(target, data, options).then((result: Writeable) => {
          this.notify('render', { target, data, options, result: result.read() });
          resolve(this.send(result.read()));
        }).catch(reject);
      } else {
        this.liquid.render(target, data, options).then((result: Writeable) => {
          this.notify('render', { target, data, options, result: result.read() });
          resolve(this.send(result.read()));
        }).catch(reject);
      }
    });
  }

  public send(body?: any): PaxResponse {
    this.expressResponse.send(body);
    this.notify('send', { body });
    return this;
  }

  public sendFile(path: string, options: any, fn: Errback): void {
    if (options === undefined) {
      if (fn === undefined) {
        this.expressResponse.sendFile(path);
      } else {
        this.expressResponse.sendFile(path, fn);
      }
    } else {
      if (fn === undefined) {
        this.expressResponse.sendFile(path, options);
      } else {
        this.expressResponse.sendFile(path, options, fn);
      }
    }
    this.notify('sendFile', { path, options, fn });
  }

  public sendStatus(statusCode: number): PaxResponse {
    this.expressResponse.sendStatus(statusCode);
    this.notify('sendStatus', { statusCode });
    return this;
  }

  public set(field: string, value?: string | string[]): PaxResponse {
    if (value === undefined) {
      this.expressResponse.set(field);
    } else {
      // This is kind of stupid but it is required for the TS typechecking to determine
      // which function to call
      if (Array.isArray(value)) {
        this.expressResponse.set(field, value);
      } else {
        this.expressResponse.set(field, value);
      }
    }
    this.notify('set', { field, value });
    return this;
  }

  public status(code: number): PaxResponse {
    this.expressResponse.status(code);
    this.notify('status', { code });
    return this;
  }

  public type(type: string): PaxResponse {
    this.expressResponse.type(type);
    this.notify('type', { type });
    return this;
  }

  public vary(field: string): PaxResponse {
    this.expressResponse.vary(field);
    this.notify('vary', { field });
    return this;
  }
}
